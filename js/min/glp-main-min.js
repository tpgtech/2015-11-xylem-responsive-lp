var main = (function () {
    'use strict';

    var _bootstrapify,  // Form
        _fixCheckboxes, // Form
        _fixRadio, // Form
        _initialize, // No Form
        _initializeForm,
        _mktoOnSuccess, // Form
        _mktoOnValidate, // Form
        _updateVisible; // No Form
       

    _bootstrapify = function bootstrapify() {
	    
		var boColumns,
			boAsterix,
			boButtonClass,
			boHideLabels;
			 
		if ($.isNumeric(_pageJS.bootstrapifyOptions.columnCount)){
			boColumns = _pageJS.bootstrapifyOptions.columnCount;
		} else {
			boColumns = 12;
		}
		
		if (_pageJS.bootstrapifyOptions.asteriskHide !== undefined){
			boAsterix = _pageJS.bootstrapifyOptions.asteriskHide;
		} else {
			boAsterix = true;
		}
		
		if (_pageJS.bootstrapifyOptions.hideLabels !== undefined){
			boHideLabels = _pageJS.bootstrapifyOptions.hideLabels;
		} else {
			boHideLabels = false;
		}
		
		// if ($.isPlainObject(_pageJS.bootstrapifyOptions.))
		
		// = (if (_pageJS.bootstrapifyOptions.columnCount !== undefined)
	    
        // Remove mktoForm class to render Marketo's default styles useless.
        $('form').removeClass('mktoForm');

        // Remove superfluous DOM elements.
        $('.mktoOffset, .mktoGutter, .mktoClear').remove();
        
        // Remove ".mktoasterix" (Seriously, that spelling?) if variable = true
        if (boAsterix === true){
	    	$('.mktoAsterix').remove(); 
        }
        
        // Remove Labels and move them to Placeholders
        if (boHideLabels === true){
	        $("form :input").each(function(index, elem) {
		        var eId = $(elem).attr("id");
				var label = null;
				if (eId && (label = $(elem).parents("form").find("label[for=" + eId + "]")).length === 1) {
					$(elem).attr("placeholder", $(label).html());
					$(label).remove();
        		}
    		});
    		if (boAsterix === true){
				$('input.mktoRequired').each(function (index, value){
				var labelValue = $(value).attr('placeholder');
				$(value).attr('placeholder','* '+labelValue);
				});
	    	}
        }
        

        

        // Add Bootstrap form classes to form elements
        $('.mktoFormRow, .mktoButtonRow').addClass('form-group').addClass('row');

        $('input:not(:checkbox, :radio, :hidden), select, textarea').addClass('form-control');

        $('.mktoButton').addClass('btn btn-lg btn-block '+_pageJS.bootstrapifyOptions.clientButtonClass);

        $('.mktoFormCol, .mktoFieldWrap').addClass('col-sm-'+boColumns);

        // Allow for multiple form columns within a row.
        $('.mktoFormRow').each(function (index, value) {
            var cols;

            cols = $(value).find('.mktoFormCol').length;
            if (cols > 1) {
                $(value).find('.mktoFormCol').removeClass('col-sm-'+boColumns).addClass('multicol').addClass('col-xs-' + (boColumns / cols));

                $(value).children('div.multicol:not(:first,:last)').css('padding', '0');

                $(value).children('div.multicol:last').css('padding-left', '0');

                $(value).children('div.multicol:first').css('padding-right', '0');
            }
        });
        
        $('.mktoErrorMsg').css('color','red');
        
        // Remove any and all inline styles (TODO will have to be done onresize as well)
        var _removeStyle = function removeStyle(){
        $('form').find('*[style]').removeAttr('style');
        $('form').removeAttr('style');
        };
        
        _removeStyle();
        // Set window.resize handler for subsequent resizes.
        $(window).resize(_removeStyle);
        
    };
	// TODO Move these functions into Bootstrapify? They are dependent on it.
    _fixCheckboxes = function fixCheckboxes() {
        $('input:checkbox').each(function (index, value) {
            var cbParent,
                cbLabelContent,
                cbLabel;

            cbParent = $(value).parent().parent();
            cbLabelContent = cbParent.children('label').html();
            cbParent.children('label').remove();
            cbParent.children().children('label').html(cbLabelContent);
            cbLabel = $(value).next();
            $(value).detach();
            $(cbLabel).prepend(value);
            $(cbLabel).wrap('<div class="checkbox"></div>');
        });
    };

    _fixRadio = function fixRadio() {
        $('input:radio').each(function (index, value) {
            var rbLabel;
            rbLabel = $(value).next();
            $(value).detach();
            $(rbLabel).prepend(value);
            $(rbLabel).wrap('<div class="radio"></div>');
        });
    };

    _initialize = function initialize() {
    	$(document).ready(function(){
			    
			    // Split Hero Text on Mobile
			    var _mobileMovements = function mobileMovements(){
				    
				   var a, b, c, d, e, f;
					    a = $('#hero-text');
					    b = $('#main .container');
					    c = $('#hero').find('.row');
					    
					    d = $('#bh-title-points');
					    e = $('#blue-highlight').find('.container');
					    f = $('#blue-highlight-text');
					    
				    if ($(window).width() < 768){
					    $(a).detach();
					    $(b).prepend(a);
					    $(a).wrap('<div class="row"></div>');					    
					    
					 }
					 else if ($(window).width() < 991) {						 
						 $(d).detach();
						 $(e).prepend(d);
						 
					 }
					 else {
						 $(a).detach();
						 $(c).append(a);
						 
						 $(d).detach();
						 $(f).prepend(d);
					 } 
				};
				
				_mobileMovements();
				
/*
				function resizedw(){
					_mobileMovements();
				}

				var doit;
				window.onresize = function(){
					clearTimeout(doit);
					doit = setTimeout(resizedw, 100);
				};
*/
				
				// $(window).resize(_mobileMovements);
				
			   	});
    
    };

    _initializeForm = function initializeForm() {

        $('div.mktoForm').removeClass('mktoForm');

        _bootstrapify();
        _fixCheckboxes();
        _fixRadio();

        // Utilize Chosen jQuery plugin on multi-select fields (https://github.com/harvesthq/chosen).
        $('select[multiple=multiple]').chosen();			
		
        _initialize();

        // TODO: How to prevent displaying the page until all JS is run.
        // document.getElementsByTagName("html")[0].style.visibility = "visible";
    };

    _mktoOnSuccess = function mktoOnSuccess() {

    };

    _mktoOnValidate = function mktoOnValidate(form) {
        // custom validation if necessary
        // return true or false
        return true;
    };

    _updateVisible = function updateVisible(hideable) {
        $.each(hideable, function (index, value) {
            if (value.show === 'false') {
                $(value.id).remove();

                if (value.updateId !== undefined && value.updateId.length > 0) {
                    $(value.updateId).removeClass(value.removeClass).addClass(value.addClass);
                }
            }
        });
    };

    return Object.freeze({
        initialize: _initialize,
        initializeForm: _initializeForm,
        mktoOnSuccess: _mktoOnSuccess,
        mktoOnValidate: _mktoOnValidate
    });
}());

// @codekit-append "glp-init.js";

/*jslint
browser, es6, fudge
*/

/*global
console, document, window, jQuery, MktoForms2, $, main
*/

$(document).ready(function () {
    'use strict';

    if (typeof MktoForms2 !== 'undefined' && $.isPlainObject(MktoForms2) === true) {
        MktoForms2.whenReady(function (form) {
            main.initializeForm();

            form.onValidate(function () {
                var submittable;

                submittable = false;
                submittable = main.mktoOnValidate(form);

                form.submittable(submittable);
            });

            form.onSuccess(main.mktoOnSuccess);
        });
    } else {
        main.initialize();
    }
});

