/*jslint
browser, es6, fudge
*/

/*global
console, document, window, jQuery, MktoForms2, $, main
*/

$(document).ready(function () {
    'use strict';

    if (typeof MktoForms2 !== 'undefined' && $.isPlainObject(MktoForms2) === true) {
        MktoForms2.whenReady(function (form) {
            main.initializeForm();

            form.onValidate(function () {
                var submittable;

                submittable = false;
                submittable = main.mktoOnValidate(form);

                form.submittable(submittable);
            });

            form.onSuccess(main.mktoOnSuccess);
        });
    } else {
        main.initialize();
    }
});